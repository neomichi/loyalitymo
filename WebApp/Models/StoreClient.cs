﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using WebApp.Models;
using MongoDB.Bson.Serialization.Attributes;

namespace WebApp.Models
{
    public class StoreClient
    {
        public ObjectId Id { get; set; }
        public int StoreClientId { get; set; }
        public string Name { get; set; }
        public List<Store> Stores { get; set; } = new List<Store>();

    }
}
