﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace WebApp.Models
{

    public class Store
    {
        public ObjectId Id { get; set; }
        public int StoreId { get; set; }
        public string StoreName { get; set; }
        public List<StoreClient> ClientToStore { get; set; } = new List<StoreClient>();
    }
}
