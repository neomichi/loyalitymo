﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using MongoDB.Bson;
using WebApp.Code;
using WebApp.Models;

namespace WebApp.Controllers
{
    [Produces("application/json")]
    [Route("api/client")]
    public class StoreClientApiController : Controller
    {
        private readonly DataAccess _context;

        public StoreClientApiController()
        {
            _context = new DataAccess();
        }

        [HttpGet("CreateClient/{name}")]
        public IActionResult GetCreateClient(string name)
        {
            var client = _context.Create(new StoreClient() { Name = name });
            return Ok(client);
        }

        [HttpGet("")]
        public IEnumerable<StoreClient> GetClients()
        {
            return _context.GetStoreClients();
        }


        [HttpGet("Count")]
        public long GetCountClient()
        {
            return _context.GetCountStoreClients();
        }

        //api/client/one/1
        [HttpGet("one/{id:int}")]
        public StoreClient GetCountClient(int id)
        {
            return _context.GetStoreClientById(id);
        }



        [HttpGet("Create5Client")]
        public IActionResult GetCreateClient()
        {
            for (var i = 0; i < 5; i++)
            {
                _context.Create(new StoreClient() {StoreClientId = i,Name = string.Format("client{0}", i) });
            }
            return Ok("готово");
        }

        [HttpGet("CreateStore")]
        public IActionResult GetCreateStore()
        {
          _context.Create(new Store() { StoreName = "33 пингвина",StoreId = 1});
          _context.Create(new Store() { StoreName = "баскин и робинс", StoreId = 2 });
            return Ok("готово");
        }


        [HttpGet("ClientAddStore")]
        public IActionResult ClientAddStore(int client, int storeId)
        {
            _context.StoreClientAddStore(client, storeId);
            return Ok("готово");
        }

        //[HttpGet("{id}")]
        //public IActionResult GetClient([FromRoute] string id)
        //{
        //    var client = _context.GetStoreClientById(new ObjectId(id));

        //    return client == null ? (IActionResult) HttpNotFound() : Ok(client);
        //}


    }
}