﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using WebApp.Models;

namespace WebApp.Code
{
    public class DataAccess
    {
        MongoClient _client;
        MongoServer _server;
        MongoDatabase _db;

        public DataAccess()
        {
            _client = new MongoClient("mongodb://192.168.56.101:27017");
            _server = _client.GetServer();
            _db = _server.GetDatabase("test");
        }

        public IEnumerable<StoreClient> GetStoreClients()
        {
            return _db.GetCollection<StoreClient>(Constant.StoreClients).FindAll().SetLimit(50);
        }

        /// <summary>
        /// добавляем пользователю магазин
        /// </summary>
        /// <param name="storeClientid"></param>
        /// <param name="storeid"></param>
        public void StoreClientAddStore(int storeClientid, int storeid)
        {

            var findStory= Query<Store>.EQ(item => item.StoreId, storeid);
            var store = _db.GetCollection<Store>(Constant.Store).FindOne(findStory);

            var query = Query<StoreClient>.EQ(item => item.StoreClientId, storeClientid);
            var update = Update<StoreClient>.Push(x => x.Stores, store);
            _db.GetCollection<StoreClient>(Constant.StoreClients).Update(query, update);

          ;
        }

        /// <summary>
        /// отнимаем у пользователя магазин
        /// </summary>
        /// <param name="storeClientid"></param>
        /// <param name="storeid"></param>
        public void StoreClientRemoveStore(ObjectId storeClientid, ObjectId storeid)
        {
            var client = _db.GetCollection<StoreClient>(Constant.StoreClients).FindOneById(storeClientid);
            client.Stores.Remove(_db.GetCollection<Store>(Constant.Store).FindOneById(storeid));
        }


        public long GetCountStoreClients()
        {
            return _db.GetCollection<StoreClient>(Constant.StoreClients).Count();
        }

        public StoreClient GetStoreClientById(int id)
        {
            var res = Query<StoreClient>.EQ(p => p.StoreClientId, id);
            return _db.GetCollection<StoreClient>(Constant.StoreClients).FindOne(res);
        }

        public StoreClient Create(StoreClient p)
        {
            _db.GetCollection<StoreClient>(Constant.StoreClients).Save(p);
            return p;
        }

        public void Update(ObjectId id, StoreClient p)
        {
            p.Id = id;
            var res = Query<StoreClient>.EQ(pd => pd.Id, id);
            var operation = Update<StoreClient>.Replace(p);
            _db.GetCollection<StoreClient>(Constant.StoreClients).Update(res, operation);
        }
        public void Remove(ObjectId id)
        {
            var res = Query<StoreClient>.EQ(e => e.Id, id);
            var operation = _db.GetCollection<StoreClient>(Constant.StoreClients).Remove(res);
        }


        public Store Create(Store p)
        {
            _db.GetCollection<Store>(Constant.Store).Save(p);
            return p;
        }


    }
}
